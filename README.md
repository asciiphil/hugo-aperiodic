This is the theme I use with [Hugo][] for [my website][aperiodic].

  [Hugo]: https://gohugo.io/
  [aperiodic]: http://aperiodic.net/pip/

It's a hodgepodge of things, largely based on my old design, which was
also very ad-hoc.

The design is minimally responsive.  It's dark-mode only.  It includes
KaTeX support.  All resources are hosted locally.

Most components are MIT-licensed.  KaTeX and the fonts have their own
licenses.

Special page types:

 * `frontpage` – Use for the main page of the website.
 * `section` – A blog section/category list page.
 * `index` – Used for a section/list page when you don't want the
   automatically-generated list of subpages.  In other words, this page's
   content provides the index.
 * `post` – A blog post.
 * `page` – A static page that should be viewed as standalone content, not
   a blog post.
 * `link` – A special type of blog post that is a link to another URL.

Add `unlisted: true` to a page's front matter to exclude it from RSS.
This also prevents it from showing up in the breadcrumb list.

To the usual social media accounts, this adds:
 * bookwyrm
 * gitlab
 * librarything
 * mastodon
 * pinboard
 * stackexchange

Note that Bookwyrm and Mastodon are federated; you must give your account
for each as "user@instance", e.g.:

    [social]
    mastodon = "xkcd@mastodon.xyz"

As with the Stack Overflow item, you must use your Stack Exchange user id.

There are a few special classes that can be added to elements to change
their appearance:

 * `c2sc` – Changes all text to small caps (including capital letters).
 * `monospace` – Uses the monospace font for the text.

The formatting assumes my personal sentence spacing style, which is to put
two spaces after a period in the source text.  That gets translated into
slightly wider spacing between sentences than between words.  (Note that
the webpage rendering shows *less* than two spaces between sentences.
It's just a little more than one space.)
