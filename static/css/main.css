/***
 *** Basic colors and typography
 ***/

:root {
    /* Colors */
    /* Largely adapted from https://reasonable.work/colors/ */
    --body-bg-color: #222;
    --body-fg-color: #e2e2e2;
    --rule-color: #3e3e3e;
    --anchor-color: #6f6f6f;  /* The waymarkers next to section titles */
    --link-fg-color: #f6f6f6;
    --link-decoration-color: #8b8b8b;
    --link-active-color: #ffdce5;
    --sidebar-fg-color: #8b8b8b;
    --code-inline-fg-color: #e0ffd9;
    --code-block-fg-color: #72ff6c;
    --code-block-bg-color: #3e3e3e;
    --kbd-border-color: #6f6f6f;
    --focus-outline-color: #9b70ff;
    --th-bg-color: #3e3e3e;
    --draft-color: #ff4647;  /* For the label on draft posts */

    /* Fonts */
    --body-font: "Bitter Variable", "Bitter", "Arial", sans-serif;
    --title-font: "Montserrat Variable", "Montserrat", "Arial", sans-serif;
    --code-font: "Iosevka Fixed Slab Web", monospace;
    --emoji-font: "Noto Emoji", sans-serif;
    /* How much to scale the code font in pre blocks to get them to be nominally 80 characters wide. */
    --pre-font-scale: 0.98;

    /* Vertical Layout

       The vertical layout is based on visual "beats".  The baseline of
       every bit of text should be aligned so that the distance between
       adjacent baselines is a multiple of the base beat width.  This
       creates a sense of underlying rhythm to the page.

       In print publishing, it's common for the beat to be equal to the
       body text's line height.  This ensures that lines of text on
       different pages always line up with each other.

       On the web, we have a little more freedom, though the body text
       line height ought to be an integer multiple of the beat width.  For
       this blog, I've chosen to have the beat be half the line height.
       There are separate beat widths for the main text column and the
       sidebars, since the base text size for the sidebars is smaller.

       To effect this:
        * All vertical margins should be integer multiple of the beat
          width.
        * All text that's not 1rem tall should have padding (usually top
          padding) to bring the text (plus its line height!) to a height
          that is a multiple of the beat width.
     */
    --base-line-height: 1.45;
    --sidebar-scale: 0.83;
    --main-vertical-beat: calc(1rem * var(--base-line-height) / 2);
    --side-font-size: calc(1rem * var(--sidebar-scale));
    --side-vertical-beat: calc(var(--sidebar-scale) * var(--main-vertical-beat));
    /* Extra vertical layout values that are used more than once. */
    --side-h2-size: 1rem;
    --side-h2-text-space: 2 * var(--side-vertical-beat);
    --side-h2-padding-top: calc(var(--side-h2-text-space) - var(--base-line-height) * var(--side-h2-size));
    --side-h2-vertical-margin: var(--side-vertical-beat);
    --toc-max-height: calc(100vh - (2 * var(--side-h2-vertical-margin) + var(--side-h2-text-space)));
}

html {
    font-family: var(--body-font);
    font-variant-numeric: oldstyle-nums;
    text-rendering: optimizeLegibility;
    font-size: 18px;  /* If this changes, all media queries need to be recalculated. */
    line-height: var(--base-line-height);
    hyphens: auto;
}

body {
    background-color: var(--body-bg-color);
    color: var(--body-fg-color);
}

#sitename,
h1,
.subtitle,
.sidebar h2 {
    font-family: var(--title-font);
    font-variant-numeric: normal;
}

#sitename,
#sitename a,
h1, h2, h3, h4 {
    font-variant-numeric: normal;
    font-weight: 700;
}

p, ol, ul {
    margin: var(--main-vertical-beat) 0;
}

/* Only the outer lists need a margin around them. */
ol ol, ul ul {
    margin: 0;
}

blockquote {
    margin: var(--main-vertical-beat) 2rem;
}

#header #sitename {
    font-size: 2.5rem;
    font-weight: 700;
    margin: 0;
}
#header h1 {  /* Article title on a standalone page. */
    font-size: 1.5rem;
    text-align: left;
    margin: 0;
}
#header .subtitle {  /* Article subtitle; optional. */
    font-size: 1rem;
    font-weight: 400;
    font-style: italic;
    margin: 0;
}
.section-title h1,
.post-title h1 {  /* Article title in a heading block on a multi-article page. */
    font-size: 1.5rem;
    text-align: left;
    margin: 0;
}
.section-title .subtitle,
.post-title .subtitle {  /* Article subtitle; optional. */
    font-size: 1rem;
    font-weight: 400;
    font-style: italic;
    margin: 0;
}
h2 {  /* Article section */
    font-size: 1.2rem;
    padding-top: calc(3 * var(--main-vertical-beat) - var(--base-line-height) * 1.2rem);
    margin: calc(2 * var(--main-vertical-beat)) 0;
}
h3 {  /* Article subsection */
    font-size: 1rem;
    padding-top: calc(2 * var(--main-vertical-beat) - var(--base-line-height) * 1rem);
    margin: calc(2 * var(--main-vertical-beat)) 0 var(--main-vertical-beat);
}
h4 {  /* Article subsubsection: avoid if possible */
    font-size: 1rem;
    padding-top: calc(2 * var(--main-vertical-beat) - var(--base-line-height) * 1rem);
    margin: var(--main-vertical-beat) 0;
    font-weight: 400;
    font-style: italic;
}

.draft {
    color: var(--draft-color);
}

.crisp {
    image-rendering: pixelated;
}

.c2sc {
    font-variant-caps: all-small-caps;
}

.monospace {
    font-family: var(--code-font);
    font-variant-numeric: slashed-zero lining-nums;
    font-stretch: expanded;
}

.emoji {
    font-family: var(--emoji-font);
}

hr {
    width: 50%;
    border: none;
    border-top: 0.5pt solid var(--rule-color);
}

/*
 We want the link character for each section to be centered between the
 edge of the column on the left and the edge of the text on the right.
 That *should* mean an indent of -2rem.  But testing shows that both
 Firefox and Chromium push the symbol too far to the left with that velus,
 while -1rem does the right thing.
 */
.post-title h1,
.post-body h2,
.post-body h3,
.post-body h4 {
    text-indent: -1rem;
}
.h-anchor {
    display: inline-block;
    text-align: center;
    width: 1rem;
    color: var(--anchor-color);
    font-weight: 400;
    font-variant-numeric: lining-nums tabular-nums;
    text-decoration: none;
}
a:hover .h-anchor,
a:hover.h-anchor {
    color: var(--body-fg-color);
    text-decoration: underline var(--body-fg-color) 1.2pt;
}
a:hover.h2-anchor {
    /* The underlining doesn't look right with the section symbol. */
    text-decoration: none;
}

a {
    color: var(--link-fg-color);
    /* NOTE: Using this weight introduces a whole other font to load just for
      links, but it's the best-looking approach I've found.  Weight 700
      just looks too heavy, and just underlining (at weight 400) doesn't
      look distinct enough. */
    font-weight: 400;
    text-decoration: underline var(--link-decoration-color) 1.2pt;
    text-underline-position: from-font;
}
a:active {
    color: var(--link-active-color);
    text-decoration-color: var(--link-active-color) !important;
}
a:hover {
    text-decoration: underline var(--body-fg-color) 1.2pt;
}
a:focus-visible {
    outline: var(--focus-outline-color) dotted 1.8pt;
    color: var(--body-bg-color);
    background-color: var(--body-fg-color);
    border-radius: calc(var(--main-vertical-beat) / 2);
}
a img {
    border-style: none;
}
a.external::after {
    font-size: 55%;
    transform: rotate(45deg);
    display: inline-block;
    vertical-align: super;
    color: var(--link-decoration-color);
    content: "↑";
    text-decoration: none;
}
a.external:hover::after {
    color: var(--body-fg-color);
}
a.external:active::after {
    color: var(--link-active-color);
}

.post-title a {
    font-weight: 700;
    text-decoration: none;
}
.post-title a:hover {
    text-decoration: underline var(--body-fg-color) 1.2pt;
}

.post-date {
    font-variant-numeric: lining-nums tabular-nums;
}

code, pre, tt, kbd, samp {
    font-family: var(--code-font);
    font-variant-numeric: slashed-zero lining-nums;
    hyphens: manual;
}
code {
    /* With the chosen fonts, code spans need a little padding to not feel crowded. */
    padding-inline: 0.2em;
}
code, tt, kbd, samp {
    color: var(--code-inline-fg-color);
    font-stretch: expanded;
}
pre, pre code, pre samp {
    font-stretch: normal;
    color: var(--code-block-fg-color);
    background: var(--code-block-bg-color);
}
pre, pre code {
    display: block;
    border-radius: calc(var(--main-vertical-beat) / 2);
}
pre code {
    padding: 0;
}
pre {
    padding: calc(var(--main-vertical-beat) / 2);
    max-width: 100%;
    overflow-x: auto;
    font-size: calc(1rem * var(--pre-font-scale));;
}
kbd {
    border: 0.5pt solid var(--kbd-border-color);
    border-radius: 0.2em;
    font-weight: 700;
}
.katex-display {
    max-width: 100%;
    overflow: auto hidden;
}

.article-separator {
    margin: 3rem auto;
}

/***
 *** Layout
 ***/

body {
    display: grid;
    grid-template-columns: 1fr 12rem minmax(39rem, 44rem) minmax(11rem, 19rem) 1fr;
    margin: 0;
}

#header {
    grid-column: 3;
    grid-row: 1;
    margin: calc(4 * var(--main-vertical-beat)) 2rem var(--main-vertical-beat);
}
#sitename {
    margin-bottom: calc(2 * var(--main-vertical-beat)) !important;
}

#toc {
    grid-column: 4;
    grid-row: 2;
    margin: var(--side-vertical-beat) 1rem 0;
    padding: var(--side-vertical-beat) 0 0;
    position: sticky;
    top: 0;
}
#TableOfContents {
    max-height: var(--toc-max-height);
    overflow-y: auto;
}

#content {
    grid-column: 3;
    grid-row: 2 / 5;
    margin: 0;
    padding: 0;
    min-width: 0;
}

#sidebar {
    grid-column: 2;
    grid-row: 2 / 4;
    margin: var(--side-vertical-beat) 1rem 0;
    padding: var(--side-vertical-beat) 1rem var(--side-vertical-beat) 0;
    border-right: solid 0.5pt var(--rule-color);
}

#footer {
    grid-column: 3;
    grid-row: 5;
    margin: calc(2 * var(--main-vertical-beat)) 2rem;
    padding-top: var(--main-vertical-beat);
    color: var(--sidebar-fg-color);
    border-top: solid 0.5pt var(--rule-color);
}

.sidebar {
    color: var(--sidebar-fg-color);
    font-size: var(--side-font-size);
}

#footer a,
.sidebar a {
    color: var(--sidebar-fg-color);
    text-decoration: none;
    font-weight: 400;
}
#footer a:hover,
.sidebar a:hover {
    color: var(--body-fg-color);
    text-decoration: underline var(--body-fg-color);
}
.sidebar a:focus-visible {
    color: var(--body-bg-color);
}

.sidebar h2 {
    clear: left;
    font-size: var(--side-h2-size);
    font-weight: 600;
    padding-top: var(--side-h2-padding-top);
    margin: var(--side-h2-vertical-margin) 0;
}
#sidebar {
    text-align: right;
}

.sidebar ul {
    margin-top: var(--side-vertical-beat);
    margin-bottom: calc(2 * var(--side-vertical-beat));
    list-style: none;
}

.sidebar ul ul {
    margin-top: 0;
    margin-bottom: 0;
    list-style: none;
    color: #888;
}
#sidebar ul {
    padding-left: 0;
}
#sidebar ul ul {
    padding-right: 0rem;
}
#sidebar ul ul li::after {
    content: " ◀";
}
#toc ul {
    padding: 0 0 0 1em;
}
#toc ul ul {
    padding-left: 0.7rem;
}
#toc.frontpage ul {
    list-style: "◆ ";
}
#toc ul {
    list-style: "§ ";
}
#toc ul ul {
    list-style: "# ";
}
#TableOfContents ul {
    margin-top: 0;
}

.post, .section {
    margin: 1.5rem 0;
}

.post-title, .section-title {
    padding: 0 2rem;
    margin-bottom: calc(2 * var(--main-vertical-beat));
}

.post-body, .section-body {
    padding: 0 2rem;
}

.post-body ul,
.post-body ol,
.section-body ul,
.section-body ol {
    padding-left: 2rem;
}

.post-meta {
    font-size: 0.8rem;
    padding-bottom: calc(2 * var(--main-vertical-beat) - var(--base-line-height) * 0.8rem);
    margin: 0;
    color: var(--sidebar-fg-color);
}
.post-meta a {
    font-weight: 400;
    color: var(--sidebar-fg-color);
    text-decoration: none;
}
.post-meta a:hover {
    color: var(--body-fg-color);
    text-decoration: underline var(--link-decoration-color);
}
.post-meta a:focus-visible {
    color: var(--body-bg-color);
}

/***
 *** Responsive Layout
 ***/

/*
 Media queries use the browser's base font size, not the one we've set
 above on the HTML element.  :(

 So we just have to calculate everything ourselves in px.

 1rem == 18px
 */
/* Width of the full desktop layout: 12rem + 39rem + 11rem = 1116px */
/* Max width with just the TOC: 39px + 11px == 900px */
/* If we get too narrow for the full layout, first move the sidebar to the
   bottom. */
@media (min-width: 900px) and  (max-width: 1116px) {
    body {
        grid-template-columns: 1fr minmax(39rem, 42rem) minmax(11rem, 19rem) 1fr;
    }
    #header {
        grid-column: 2;
        grid-row: 1;
    }
    #toc {
        grid-column: 3;
        grid-row: 2;
    }
    #content {
        grid-column: 2;
        grid-row: 2 / 4;
    }
    #sidebar {
        grid-column: 2;
        grid-row: 4;
    }
    #footer {
        grid-column: 2;
        grid-row: 5;
    }
}
@media (max-width: 1116px) {
    /* Sidebar-at-the-bottom stuff that doesn't vary with the number of sidebar columns. */
    #sidebar {
        display: grid;
        margin: calc(2 * var(--main-vertical-beat)) 2rem 0;
        padding: var(--main-vertical-beat) 0 0;
        font-size: 1rem;
        border-top: solid 0.5pt var(--rule-color);
        border-right: none;
        text-align: inherit;
    }
    .sidebar h2 {
        font-size: 1.2rem;
        padding-top: calc(2 * var(--main-vertical-beat) - var(--base-line-height) * 1.2rem);
        margin: var(--main-vertical-beat) 0;
    }
    .sidebar ul {
        margin-top: var(--main-vertical-beat);
        margin-bottom: calc(2 * var(--main-vertical-beat));
    }
    #sidebar ul ul li::after {
        content: none;
    }
    #sidebar ul ul li::before {
        content: "▶ ";
    }
}
/* Now just hide the TOC. */
@media (max-width: 900px) {
    body {
        grid-template-columns: 1fr minmax(auto, 42rem) 1fr;
    }
    #header {
        grid-column: 2;
        grid-row: 1;
    }
    #toc {
        display: none;
    }
    #content {
        grid-column: 2;
        grid-row: 2;
    }
    #sidebar {
        grid-column: 2;
        grid-row: 3;
    }
    #footer {
        grid-column: 2;
        grid-row: 4;
    }
}
/*
 In the sidebar-at-the-bottom configuration, we want at least 10.5rem per
 column (this gives enough room for the larger-font content plus some
 inter-column padding).  There's also 2rem margin on each side, so that
 gives a minimum three-column width of: 3 * 10.5rem + 2 * 2rem = 639rem
 */
@media (min-width: 639px) and (max-width: 1116px) {
    #sidebar {
        grid-template-columns: repeat(3, minmax(10.5rem, auto));
    }
}
@media (max-width: 639px) {
    #sidebar {
        grid-template-columns: repeat(2, minmax(10.5rem, auto));
    }
}
/* Empirical: once the viewport drops below 572px, reducing the font size
   keeps more content in a single row without being unreadably small. */
@media (max-width: 572px) {
    html { font-size: 16px; }
    pre { font-size: 0.775rem; }
}

img,
picture,
video {
    max-width: 100%;
    height: auto;
}

/***
 *** Tables
 ***/

table {
    border-collapse: collapse;
    display: block;
    margin: calc(2 * var(--main-vertical-beat)) 0;
    overflow: auto;
    width: 100%;
}

td,
th {
    text-align: left;
    padding: calc(var(--main-vertical-beat) / 2) 0.75rem;
}

td {
    border-bottom: 0.5pt solid var(--rule-color);
}

th {
    --font-size: 0.9rem;
    --vertical-padding: calc(3 * var(--main-vertical-beat) - var(--base-line-height) * var(--font-size));
    font-size: var(--font-size);
    font-weight: 700;
    /* We distribute slightly more space to the bottom because it looks a little better that way. */
    padding-top: calc(var(--vertical-padding) * 2 / 5);
    padding-bottom: calc(var(--vertical-padding) * 3 / 5);
    background-color: var(--th-bg-color);
}

/***
 *** Misc
 ***/

.pagination {
    display: inline-block;
    padding: 0;
    color: var(--sidebar-fg-color);
}
#content .pagination {
    padding-left: 2em;
}
#toc .pagination {
    padding: 0;
}

ul.pagination {
    text-indent: 0 !important;
}
.pagination li {
    float: left;
    display: inline-block;
    margin: 0 0.25rem;
}

.pagination a {
    text-decoration: none;
    color: var(--sidebar-fg-color);
    font-weight: 400;
}

.pagination .active a {
    color: var(--body-fg-color);
    font-weight: 700;
}

figure.right {
    float: right;
    clear: right;
    text-align: right;
    max-width: 45%;
    margin: var(--main-vertical-beat) 0 var(--main-vertical-beat) 2em;
}

figure.left {
    float: left;
    clear: left;
    text-align: left;
    max-width: 45%;
    margin: var(--main-vertical-beat) 2em var(--main-vertical-beat) 0;
}

figure.center {
    margin: var(--main-vertical-beat) auto;
    text-align: center;
    max-width: 100%;
}

figure {
    display: table;
}
figure img {
    max-width: 100%;
}
figcaption {
    display: table-caption;
    caption-side: bottom;
}
figcaption p {
    --line-height: 1.2;
    --font-size: 0.9rem;
    --caption-vertical-beat: calc(var(--line-height) * var(--font-size) / 2);
    margin-top: calc(var(--caption-vertical-beat) / 2);
    width: 100%;
    font-size: var(--font-size);
    font-style: italic;
    line-height: var(--line-height);
}
